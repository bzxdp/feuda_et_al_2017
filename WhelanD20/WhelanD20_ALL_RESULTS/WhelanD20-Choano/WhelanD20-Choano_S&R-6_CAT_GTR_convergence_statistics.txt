tracecomp -x 6000 D20_SUSKO_CHOA_CAT_GTR1 D20_SUSKO_CHOA_CAT_GTR2

initialising random
seed was : 992988

setting upper limit to : 17184
name                effsize	rel_diff

loglik              691		0.0509579
length              2162		0.0176217
alpha               1221		0.102557
Nmode               3160		0.0454813
statent             613		0.0813251
statalpha           5687		0.0742495
rrent               1381		0.0456704
rrmean              8749		0.0017183

bpcomp -x 6000 20 D20_SUSKO_CHOA_CAT_GTR1 D20_SUSKO_CHOA_CAT_GTR2

initialising random
seed was : 146224


D20_SUSKO_CHOA_CAT_GTR1.treelist : 559 trees
D20_SUSKO_CHOA_CAT_GTR2.treelist : 559 trees

maxdiff     : 0.0733453
meandiff    : 0.001059

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre