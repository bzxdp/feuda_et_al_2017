bpcomp -x 8000 20 D20_KOSIOL_CHOANO_CATGTR_3 D20_KOSIOL_CHOANO_CATGTR_4

initialising random
seed was : 868728


D20_KOSIOL_CHOANO_CATGTR_3.treelist : 745 trees
D20_KOSIOL_CHOANO_CATGTR_4.treelist : 745 trees

maxdiff     : 0.0630872
meandiff    : 0.000885821

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre

 tracecomp -x 8000 20 D20_KOSIOL_CHOANO_CATGTR_3 D20_KOSIOL_CHOANO_CATGTR_4        

initialising random
seed was : 635760

setting upper limit to : 22913
name                effsize	rel_diff

loglik              2743		0.141101
length              6976		0.00714797
alpha               7666		0.133183
Nmode               2249		0.075602
statent             10607		0.147063
statalpha           1666		0.133726
rrent               6530		0.0490334
rrmean              3590		0.0305112