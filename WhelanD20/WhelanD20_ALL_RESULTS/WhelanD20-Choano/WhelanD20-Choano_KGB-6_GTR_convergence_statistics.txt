bpcomp -x 6000 20 16672 D20_KOSIOL_CHOANO_GTR_1 D20_KOSIOL_CHOANO_GTR_2

initialising random
seed was : 678870


D20_KOSIOL_CHOANO_GTR_1.treelist : 533 trees
D20_KOSIOL_CHOANO_GTR_2.treelist : 533 trees

maxdiff     : 0.0131332
meandiff    : 0.000232583

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre


tracecomp -x 6000  D20_KOSIOL_CHOANO_GTR_1 D20_KOSIOL_CHOANO_GTR_2

initialising random
seed was : 104888

setting upper limit to : 16672
name                effsize	rel_diff

loglik              6911		0.00265551
length              6739		0.00380115
alpha               6369		0.00889739
Nmode               10672		0
statent             2347		0.0413598
statalpha           10672		0
rrent               4156		0.0101127
rrmean              7348		0.00840727