tracecomp -x 500 LG1 LG2

initialising random
seed was : 249452

setting upper limit to : 1740
name                effsize	rel_diff

loglik              841		0.0291019
length              603		0.147486
alpha               434		0.0207493
Nmode               1240		0
statent             97		0.0477979
statalpha           1066		0.0209601
[bzxdp@newblue4 OPISTO]$ bpcomp -x 500 10 LG1 LG2

initialising random
seed was : 194014


LG1.treelist : 124 trees
LG2.treelist : 124 trees

maxdiff     : 0
meandiff    : 0

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre