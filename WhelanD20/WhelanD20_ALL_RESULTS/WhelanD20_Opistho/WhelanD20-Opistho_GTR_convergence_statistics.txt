[bzxdp@newblue2 OPISTO]$ bpcomp -x 500 10 1679 GTR1 GTR2

initialising random
seed was : 922574


GTR1.treelist : 117 trees
GTR2.treelist : 116 trees

maxdiff     : 0
meandiff    : 0

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre

[bzxdp@newblue2 OPISTO]$ tracecomp -x 500  GTR1 GTR2

initialising random
seed was : 650861

setting upper limit to : 1665
name                effsize	rel_diff

loglik              214		0.0136347
length              386		0.150067
alpha               614		0.0358728
Nmode               1165		0
statent             98		0.182049
statalpha           1165		0.0253451
rrent               183		0.281729
rrmean              733		0.0186226