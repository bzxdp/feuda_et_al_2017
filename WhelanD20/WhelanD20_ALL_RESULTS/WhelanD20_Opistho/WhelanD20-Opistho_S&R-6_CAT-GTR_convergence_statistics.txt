tracecomp -x 6000 D20_Susko_CATGTR_1 D20_Susko_CATGTR_2

initialising random
seed was : 449177

setting upper limit to : 16087
name                effsize	rel_diff

loglik              468		0.0402323
length              1229		0.0269976
alpha               514		0.0930021
Nmode               5694		0.129449
statent             626		0.0580186
statalpha           5449		0.00631346
rrent               1623		0.0495398
rrmean              7955		0.0282258

bpcomp -x 6000 20 D20_Susko_CATGTR_1 D20_Susko_CATGTR_2

initialising random
seed was : 474647


D20_Susko_CATGTR_1.treelist : 504 trees
D20_Susko_CATGTR_2.treelist : 505 trees

maxdiff     : 0.214769
meandiff    : 0.00181329

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre