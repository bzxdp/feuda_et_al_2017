tracecomp -x 5000  D20_SUSKO_HOLO_GTR1 D20_SUSKO_HOLO_GTR2

initialising random
seed was : 208258

setting upper limit to : 11673
name                effsize	rel_diff

loglik              4127		0.0225003
length              3899		0.00829082
alpha               3100		0.127056
Nmode               6673		0
statent             3703		0.0253097
statalpha           6673		0
rrent               1976		0.017902
rrmean              558		0.00148495

bpcomp -x 5000 20 11673 D20_SUSKO_HOLO_GTR1 D20_SUSKO_HOLO_GTR2

initialising random
seed was : 14706


D20_SUSKO_HOLO_GTR1.treelist : 333 trees
D20_SUSKO_HOLO_GTR2.treelist : 333 trees

maxdiff     : 0.102102
meandiff    : 0.000814768

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre