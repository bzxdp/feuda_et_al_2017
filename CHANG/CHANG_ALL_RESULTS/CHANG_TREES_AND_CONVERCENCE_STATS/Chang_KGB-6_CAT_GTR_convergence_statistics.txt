bpcomp -x 6000 20 CHANG_KOSIOL_CAT_GTR1 CHANG_KOSIOL_CAT_GTR2

initialising random
seed was : 219306


CHANG_KOSIOL_CAT_GTR1.treelist : 501 trees
CHANG_KOSIOL_CAT_GTR2.treelist : 501 trees

maxdiff     : 0.0758483
meandiff    : 0.00172652

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre

 tracecomp -x 6000 CHANG_KOSIOL_CAT_GTR1 CHANG_KOSIOL_CAT_GTR2

initialising random
seed was : 792980

setting upper limit to : 16023
name                effsize	rel_diff

loglik              656		0.081461
length              7887		0.0428524
alpha               483		0.0386583
Nmode               944		0.0103323
statent             1238		0.0261809
statalpha           604		0.0769182
rrent               6134		0.0965293
rrmean              10023		0.0390221