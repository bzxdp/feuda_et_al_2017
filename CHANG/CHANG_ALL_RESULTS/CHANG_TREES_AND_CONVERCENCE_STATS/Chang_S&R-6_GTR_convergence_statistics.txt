tracecomp -x 4000  CHANG_SUSKO_GTR1 CHANG_SUSKO_GTR2

initialising random
seed was : 110541

setting upper limit to : 9148
name                effsize	rel_diff

loglik              4062		0.0212029
length              1116		0.00477982
alpha               3336		0.0408985
Nmode               5148		0
statent             887		0.0408452
statalpha           5148		0
rrent               725		0.0537088
rrmean              463		0.126003

bpcomp -x 4000 20 9148 CHANG_SUSKO_GTR1 CHANG_SUSKO_GTR2

initialising random
seed was : 625196


CHANG_SUSKO_GTR1.treelist : 257 trees
CHANG_SUSKO_GTR2.treelist : 257 trees

maxdiff     : 0.0272374
meandiff    : 0.00036076

bipartition list in : bpcomp.bplist
consensus in        : bpcomp.con.tre